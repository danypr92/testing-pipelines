[![pipeline status](https://gitlab.com/danypr92/testing-pipelines/badges/master/pipeline.svg)](https://gitlab.com/danypr92/testing-pipelines/commits/master)

Project to test the GitlabCI configuration.

Tested:

### Execute job when some files change

https://docs.gitlab.com/ee/ci/yaml/#onlychanges--exceptchanges

```yml
update:
  stage: test
  script:
    - echo 'Version changed:'
    - cat VERSION
  only:
    changes:
      - VERSION
```

### Execute job only in branches or tags

https://docs.gitlab.com/ee/ci/yaml/#onlyrefs--exceptrefs

```yml
update:
  stage: test
  script:
    - echo 'Version changed:'
    - cat VERSION
  only:
    refs:
      - tags
      - main
      - /^testing/*
      - merge-request
```

### Environments and deployments

https://docs.gitlab.com/ee/ci/environments/index.html

1. Create an environment in the Gitlab UI.
1. Update the `.gitlab-ci.yml` adding:

```yml
deploy_staging:
  stage: test
  script:
    - echo "Deploy to test server"
  environment:
    name: test
    url: https://testing.example.com
```

### Create RELEASE

https://docs.gitlab.com/ee/ci/yaml/#release

```yml
release_job:
	stage: test
	image: registry.gitlab.com/gitlab-org/release-cli:latest
	rules:
		- if: $CI_COMMIT_TAG                  # Run this job when a tag is created manually
	script:
		- echo "Running the release job."
	release:
		name: 'Release $CI_COMMIT_TAG'
		description: 'Release created using the release-cli.'
```
